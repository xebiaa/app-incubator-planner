package com.xebia.ai.planner

import com.mongodb.casbah.Imports._


trait MongoRepository extends Logging {
    protected val host = "localhost"
    protected val port = 27017
    protected val databaseName = "ai-planner"

    protected val collectionName: String

    protected def afterCollectionInitialized(collection: MongoCollection) {}

    protected lazy val connection = MongoConnection(host, port)
    protected lazy val database = connection(databaseName)
    protected lazy val collection = {
        val col = database(collectionName)

        afterCollectionInitialized(col)

        col
    }

    def count() = collection.getCount()
}
