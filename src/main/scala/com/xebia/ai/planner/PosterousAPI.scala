package com.xebia.ai.planner

import com.ning.http.client.Response

import net.liftweb.json._
import net.liftweb.json.JsonAST.{JValue, JField}
import java.text.SimpleDateFormat


class PosterousAPI extends Logging {
    private val postsUrl = "http://posterous.com/api/2/users/474988/sites/1191595/posts/public"

    def retrieveLatestPosts(handlePost: PosterousPost => Unit) {
        Http.getAsync(postsUrl) {response =>
            if (requestWasSuccessful(response)) {
                val json = JsonParser.parse(response.getResponseBody)
                val jsonPosts: List[JValue] = json.children

                jsonPosts.foreach{(jsonPost: JValue) =>
                    handlePost(extractPosterousPost(jsonPost))
                }
            }
        }
    }

    def retrieveAllPosts(handlePost: PosterousPost => Unit) {
        getAllPosts(1, handlePost)
    }


    // ========================================================================
    // Implementation details
    // ========================================================================

    private def getAllPosts(page: Int, handlePost: PosterousPost => Unit) {
        val posts: List[JValue] = getPosts(page)

        posts match {
            case head :: tail =>
                log.info("Found %d posts for page %d", posts.size, page)

                posts.foreach{(post: JValue) =>
                    handlePost(extractPosterousPost(post))
                }

                getAllPosts(page + 1, handlePost)
            case _ =>
                log.info("Found no posts for page %d, we've obviously reached the beginning of time.", page)
        }
    }

    private def getPosts(page: Int): List[JValue] = {
        val response = Http.get(postsUrl + "?page=" + page).get

        if (requestWasSuccessful(response)) {
            JsonParser.parse(response.getResponseBody).children
        } else {
            Nil
        }
    }

    private def requestWasSuccessful(response: Response): Boolean = {
        response.getStatusCode == 200 && response.getContentType.contains("application/json")
    }

    implicit private val formats = new DefaultFormats {
        override def dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss Z")
    }

    private def extractPosterousPost(jsonPost: JValue): PosterousPost = {
        // rename some fields to fit our naming standards
        val transformed = jsonPost.transform {
            case JField("display_date", x) => JField("publishedOn", x)
            case JField("full_url", x) => JField("url", x)
            case JField("body_html", x) => JField("body", x)
            case JField("last_activity", x) => JField("lastActivity", x)
            case JField("profile_pic", x) => JField("imageUrl", x)
        }

        transformed.extract[PosterousPost]
    }
}
