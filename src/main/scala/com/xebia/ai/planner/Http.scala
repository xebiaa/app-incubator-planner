package com.xebia.ai.planner

import com.ning.http.client.{Response, AsyncCompletionHandler, AsyncHttpClient}
import java.util.concurrent.Future

object Http extends Logging {
    private val asyncHttpClient = new AsyncHttpClient();
    private def defaultErrorHandler(error: Throwable): Unit = log.error(error, "Error while retrieving the latest posts from Posterous.")

    /** Synchronous get. */
    def get(url: String)(implicit errorHandler: Throwable => Unit = defaultErrorHandler): Future[Response] = {
        asyncHttpClient.prepareGet(url).execute(new AsyncCompletionHandler[Response] {
            override def onCompleted(response: Response): Response = response
            override def onThrowable(t: Throwable) {
                errorHandler(t)
            }
        })
    }

    /** Asynchronous get. */
    def getAsync(url: String)(handleResponse: Response => Unit)(implicit errorHandler: Throwable => Unit = defaultErrorHandler): Future[Int] = {
        asyncHttpClient.prepareGet(url).execute(new AsyncCompletionHandler[Int] {
            override def onCompleted(response: Response): Int = {
                handleResponse(response)

                response.getStatusCode
            }

            override def onThrowable(t: Throwable) {
                errorHandler(t)
            }
        })
    }
}
