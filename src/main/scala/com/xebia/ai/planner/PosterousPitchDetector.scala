package com.xebia.ai.planner


class PosterousPitchDetector(private val posterous: PosterousAPI, private val repository: ProjectRepository) extends Logging {
    
    def detectAndHandleNewPosterousPitches {
        posterous.retrieveAllPosts { post =>
            if (post.isPitch) {
                log.info("Detected pitch on Posterous titled: %s", post.title)

                // TODO: detect existing project, if not create one using this post.
                // TODO: wrap post in PosterousProject and save that instead
                repository.insert(post)
            }
        }
    }
}
