package com.xebia.ai.planner

import java.util.Date
import com.mongodb.casbah.Imports._
import com.novus.salat.annotations._



//case class Hacker (
//    @Key("_id") id: ObjectId = new ObjectId(),
//    firstName: String,
//    lastName: String,
//    posterousId: Long,
//
//)
//
//
//case class Project (
//    @Key("_id") id: ObjectId = new ObjectId(),
//    pitch: PosterousPost,
//    owner: Option[ObjectId]
//)



case class PosterousUser (
    id: Long,
    nickname: String,
    firstname: String,
    lastname: String,
    lastActivity: Date,
    imageUrl: String
)

case class PosterousTag (
    id: Long,
    name: String
)

case class PosterousPost (
    @Key("_id") id: Long,
    title: String,
    publishedOn: Date,
    url: String,
    user: PosterousUser,
    body: String,
    tags: List[PosterousTag]
) {
    val pitchPattern = ".*[pP]itch.*"

    def isPitch(): Boolean = title.matches(pitchPattern) || tags.exists(_.name.matches(pitchPattern))
}

