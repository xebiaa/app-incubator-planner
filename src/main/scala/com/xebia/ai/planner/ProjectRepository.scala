package com.xebia.ai.planner

import com.mongodb.casbah.Imports._
import com.mongodb.DBObject

import com.novus.salat._
import com.novus.salat.global._


class ProjectRepository extends Iterable[PosterousPost] with MongoRepository {
    protected val collectionName = "posts"

    protected override def afterCollectionInitialized(collection: MongoCollection) {
        super.afterCollectionInitialized(collection)

        log.debug("Collection '%s' initialized", collectionName)
    }

    def iterator = {
        collection.find().map((value: DBObject) => grater[PosterousPost].asObject(value))
    }

    def insert(post: PosterousPost) {
        collection.insert(grater[PosterousPost].asDBObject(post))
    }
}
