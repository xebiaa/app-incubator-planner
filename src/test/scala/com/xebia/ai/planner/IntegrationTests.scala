package com.xebia.ai.planner

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers



@RunWith(classOf[JUnitRunner])
class IntegrationTest extends FlatSpec with ShouldMatchers with Logging {

    "the PosterousPitchDetector" should "detect some pitches on Posterous" in {
        val repository = new ProjectRepository with MongoTestRepository
        val detector = new PosterousPitchDetector(new PosterousAPI, repository)

        detector.detectAndHandleNewPosterousPitches
    }
}

