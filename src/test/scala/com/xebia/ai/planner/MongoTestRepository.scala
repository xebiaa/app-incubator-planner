package com.xebia.ai.planner

import com.mongodb.casbah.Imports._


trait MongoTestRepository extends MongoRepository {
    protected override val databaseName = "unit_tests"
    protected override def afterCollectionInitialized(collection: MongoCollection) {
        super.afterCollectionInitialized(collection)

        log.debug("Dropping '%s' collection", collectionName)

        collection.drop; // drop the collection so we get a fresh one for this test
    }
}
