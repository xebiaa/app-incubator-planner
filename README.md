# The App Incubator Planner App

The AI Planner app will facilitate the process of proposing a Xebia App Incubator project, voting on it, adding people to it, reporting on it, etc. It's main purpose is to make sure that everybody (at least within Xebia) knows which projects are active, which ones are looking for new members, when and where people will work on them, etc.

## Goals for version 1

- Poll the App Incubator blog RSS feed for pitch posts
    - create a new project for every pitch post found
        - store this project in MongoDB
    - send an announcement to a mailing list (tech ? all NL ?)
- Create a web form where people can sign up to be core members or contributors of a project
    - on form submission, add those members to the project in MongoDB
- Monitor the number of core team members
    - if >= 3
        - set project status to active in mongoDB
        - notify team members that the project is active
- Keep the confluence page for the next XKE up-to-date with information about all active projects
- Create a web page listing all projects
- allow projects to be closed (i.e. status -> closed)

## After that

- integrate with Srumy or another tool to maintain a simple list of stories and/or tasks
- integrate with the blog to automatically harvest further blog posts about the project

## And after that

- world domination

